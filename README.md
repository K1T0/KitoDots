# _Contents_

- [My dotfiles inspired by DT](#My dotfiles inspired by DT)
  - [_Contents_](#My dotfiles inspired by DT#_Contents_)
  - [Apps recap](#My dotfiles inspired by DT#Apps recap)
  - [Past WMs](#My dotfiles inspired by DT#Past WMs)
    - [Qtile](#My dotfiles inspired by DT#Past WMs#Qtile)
    - [Xmonad](#My dotfiles inspired by DT#Past WMs#Xmonad)
    - [Awesomewm](#My dotfiles inspired by DT#Past WMs#Awesomewm)
  - [Actual WM](#My dotfiles inspired by DT#Actual WM)
    - [DWM](#My dotfiles inspired by DT#Actual WM#DWM)
    - [Open](#My dotfiles inspired by DT#Actual WM#Open)
  - [Other Apps](#My dotfiles inspired by DT#Other Apps)
    - [Vim](#My dotfiles inspired by DT#Other Apps#Vim)
    - [Qutebrowser](#My dotfiles inspired by DT#Other Apps#Qutebrowser)
    - [Alacritty](#My dotfiles inspired by DT#Other Apps#Alacritty)
    - [Polybar](#My dotfiles inspired by DT#Other Apps#Polybar)
  - [Special thanks to :](#My dotfiles inspired by DT#Special thanks to :)

# My dotfiles inspired by DT

## Apps recap

Theese are my dotfiles for my twms and apps i usually use on my linux machine

| App                 | Use                             |
|---------------------|---------------------------------|
| Leftwm              | Primary Tiling Window Manager   |
| Dwm                 | Backup Tiling Window Manager    |
| Openbox             | Primary Floating Window Manager |
| Alacritty           | Terminal                        |
| Qutebrowser/Firefox | Browsers                        |
| Emacs               | IDE                             |
| Nvim                | Quick Text Editor               |
| Sxhkd               | Keybindings manager             |
| Dolphin             | File manager                    |

## Past WMs

### Qtile

Qtile is a twm inspired by xmonad but written in python. It is more simple to
configure and manage but less powerfull so i left it in favor of xmonad who is "
the best seller " even if more difficult to configure

### Xmonad

Xmonad is a twm written in haskell, a very complex programming language. It is
fast, suckless, powerfull and aesthetichally beautifull at the same time,
literally all you can think is possible on xmonad so spending some hours to
learn how it works is a good thing and a great lerning experience. The big
problem with this wm is actually the haskell itself. Even now I don't undestand
something there is in my config and is not a good thing

### Awesomewm

Awesomewm was a dwm fork written in lua language. Today that project become so
big as a stand alone window manager. Problably is the most extensible of all
twm, he's got a bar and widgets library. 
Actually i don't use it anymore beacouse i can't stand lua, but i used in the
past as floating wm. He' s just beautifull and i suggest y'all to give it a try
if you know something of lua

## Actual WM

### Leftwm

Leftwm is unique. It is minimal, expandible and rusty, of course. It has all
that I need in a tiling wm, including one cool theming feature. Mainwhile, i
cannot get scratchpads to work, if someone could help me I would appreciate it.

### DWM

Dwm is one of my fevorite twm. It' s totally different from all the other
beacouse you don't configure it but you write it from source code yourself. The
problem is how old the method of writing is, in fact all the function you would
add to dwm are pre-written in patches, who are easy to install in most cases but
sometimes they are impossible or not compatible with each other, so you need to
udjust something manually. Other that is the perfect wm.

### Openbox

Openbox is a floating window manager configured in xml. I personally don't love
this choise bur i prefere this instead of lua. Actually i use it as my floating
wm, just for some chilling use or when i need to use floatings tabs.

## Other Apps

### Vim

I added some plugins for trasform nvim in a semi-ide for quick edit, with a
theme that match with my general theme (mountain). Then i installed Neovide who
is a graphical front-end for nvim and i setupped it as my primary IDE. The
difference between these two packages is not s big, but the small features
imported by neovide are cool so why don' t use it ?

### Qutebrowser

I added only a custom theme and a custom start page. The custom theme is,
obviusly, mountain, modified a bit mixing it with dracula for a better
navigation

### Alacritty

Alacritty is my terminal. I found it very quick in tasks and customizable. I
edit yaml file to match up with my mountain theme

### Polybar

Openbox and Leftwm haven't got a bar so i configured polybar to be my aesthetic panel for wms.

### Emacs
Emacs is basically my second os. I use a lot of emacs' applications, from dired-ranger to elfeed-org. I really suggest you to take a look at it.

## Special thanks to :

I need to thanks Derek Taylor ([Youtube](https://www.youtube.com/channel/UCVls1GmFKf6WlTraIb_IaJg),
[gitlab](htts://gitlab.com/dwt1)), Mental Outlaw
([Youtube](https://www.youtube.com/channel/UC7YOGHUfC1Tb6E4pudI9STA),
[Github](https://github.com/MentalOutlaw)) and Luke Smith (
[Youtube](https://www.youtube.com/channel/UC2eYFnH61tmytImy1mTYvhA),
[Github](https://github.com/LukeSmithxyz)) for heavly help and inspire me in
this "mission". Is only thanks to them, expecially the first 2, if a
16-year-old boy like me (not even English but Italian, and I take this
opportunity to report writing errors if you find them ) was able to get to
where he is with the study of linux and the love for FOSS. I need to specify
one thing: despite what I just said, this repository IS NOT a copy and paste of
theirs but a transposition of my knowledge with their files used only as an inspiration.
Another special thank go to the creator of Mountain theme
([gitlab](https://github.com/mountain-theme/Mountain.git)) beacouse when i saw
this color palette i became mad and use it instantly.
