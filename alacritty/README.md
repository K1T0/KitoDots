# Alacritty config

## What is alacritty ?

[Alacritty](https://github.com/alacritty/alacritty) is a terminal emulator who
is GPU accellerated so it is the fastest terminal you can use. It is the
successor of
[termite](https://github.com/thestinger/termite/https://github.com/thestinger/termite/)
and is written in rust.

## What is my config about?

Alacritty has very good defaults but i need to configure it to use my color
scheme and font. My config is almost solely dedicated on look and feel, but
also has some keybindings initialized into it.
