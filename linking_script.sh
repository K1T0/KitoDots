
cd $HOME  
ln -s .config/xmonad ~/.xmonad
ln -s .config/suckless-software/dwm/autostart.sh autostart.sh
ln -s .config/zshrc .zshrc
ln -s .config/bashrc .bashrc
