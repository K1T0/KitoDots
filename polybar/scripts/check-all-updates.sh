#!/bin/sh
#source https://github.com/x70b1/polybar-scripts


#updates=$(xbps-install --memory-sync --dry-run --update | grep -Fe update -e install | wc -l)

updates=$(checkupdates | wc -l)

if [ "$updates" -gt 0 ]; then
    echo " $updates"
else
    echo "0"
fi
