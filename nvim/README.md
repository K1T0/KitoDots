# Vim config

1. [About This Config](#about-this-config)
2. [Plugin manager](#plugin-manager)
3. [Themes](#themes)

## About This Config

I love it and use for any purpouse. For anybody who don' t know what vim is, I can
suggest you to take a look at this text editor beacouse is very usefull and
quick. More specificly, I use neovim with lua config.

## Plugin manager

I use Lazy and I find it very usefull and easy to use. Never think to switch to
another plugin manager

## Themes

Of course I use a theme that match with all my system and some other fancy
stuff. Actually it is onedark, but I leaved in my config gruvbox-dark too
commented to easly switch from one theme to another

