vim.g.mapleader = " " -- Make sure to set `mapleader` before lazy so your mappings are correct
vim.g.maplocalleader = "\\" -- Same for `maplocalleader`

local function map(m, k, v)
	vim.keymap.set(m, k, v, { silent = true })
end

--nvim tree
map("n","<leader><F3>","<ESC>:NvimTreeToggle<CR><ESC>",{desc="toggle nvim tree"})
map("v","<leader><F3>","<ESC>:NvimTreeToggle<CR><ESC>",{desc="toggle nvim tree"})

--split
map("n","<leader>wv", "<ESC>:vsplit<CR><ESC>",{desc="create a vertical split"})
map("n","<leader>vv", "<ESC>:split<CR><ESC>",{desc="create an horizontal split"})
map("n","<leader>=", "<ESC><C-w>=<ESC>", {desc="turn the splits back to the default size"})
map("n","<A-.>", "<C-w>>", {desc="ingrandisci lo split"})
map("n","<A-,>", "<C-w><", {desc="rimpicciolisci lo split"})
map("n","<A-=>", "<C-w>+", {desc="ingrandisci lo split (basso)"})
map("n","<A-->", "<C-w>-", {desc="rimpicciolisci lo split (basso)"})

-- windows
map("n","<leader>j", "<C-W>j", {desc="move to the window up"})
map("v","<leader>j", "<C-W>j", {desc="move to the window up"})
map("n","<leader>k", "<C-W>k", {desc="move to the window down"})
map("v","<leader>k", "<C-W>k", {desc="move to the window down"})
map("n","<leader>h", "<C-W>h", {desc="move to the window left"})
map("v","<leader>h", "<C-W>h", {desc="move to the window left"})
map("n","<leader>l", "<C-W>l", {desc="move to the window right"})
map("v","<leader>l", "<C-W>l", {desc="move to the window right"})

map("n","<leader><Left>", "<C-W>h",{desc="move to the window left"})
map("n","<leader><Right>","<C-W>l",{desc="move to the window right"})
map("n","<leader><Down>", "<C-W>j",{desc="move to the window down"})
map("n","<leader><Up>", "<C-W>k",{desc="move to the window up"})
map("v","<leader><Left>", "<C-W>h",{desc="move to the window left"})
map("v","<leader><Right>","<C-W>l",{desc="move to the window right"})
map("v","<leader><Down>", "<C-W>j",{desc="move to the window down"})
map("v","<leader><Up>", "<C-W>k",{desc="move to the window up"})

--Set <leader> + 1/2 per navigare nelle tabs
map("n","<leader>1", "<ESC>:BufferLineCyclePrev<CR><ESC>",{desc="move to the previous tab"})
map("v","<leader>1", "<ESC>:BufferLineCyclePrev<CR><ESC>",{desc="move to the previous tab"})
map("n","<leader>2", "<ESC>:BufferLineCycleNext<CR><ESC>",{desc="move to the next tab"})
map("v","<leader>2", "<ESC>:BufferLineCycleNext<CR><ESC>",{desc="move to the next tab"})
map("n","<leader>bk", "<ESC>:bdelete<CR><ESC>",{desc="close buffer"})
map("v","<leader>bk", "<ESC>:bdelete<CR><ESC>",{desc="close buffer"})


--set <leader> + a/e per andare a inizio/fine riga
map("n","<leader>e", "$",{desc="move to the end of the line"})
map("v","<leader>e", "$",{desc="move to the end of the line"})
map("n","<leader>a", "^",{desc="move to the end of the line"})
map("v","<leader>a", "^",{desc="move to the end of the line"})

--Telescope
map("n","<leader>f", "<ESC>:Telescope file_browser path=%:p:h select_buffer=true<CR>",{desc="telescope"})

--settare ge per andare a fine file
map("n","ge", "G")
map("v","ge", "G")

--dashboard
map("n","<leader>h","<ESC>:Dashboard<CR><ESC>",{desc="return to the startpage"})

--distractionless
map("n","<leader>gy", "<ESC>:TZAtaraxis<CR><ESC>",{desc="enter the distractionless mode"})

--zoom
--[[ map("n","<C-+>", "<ESC>:ZoomIn<ESC>",{desc="zoom in"})
map("v","<C-+>", "<ESC>:ZoomIn<ESC>",{desc="zoom in"})
map("n","<C-->", "<ESC>:ZoomOut<ESC> ",{desc="zoom in"})
map("v","<C-->", "<ESC>:ZoomOut<ESC> ",{desc="zoom in"})
map("n","<C-=>", "<ESC>:ZoomDefault<ESC>",{desc="zoom default"})
map("v","<C-=>", "<ESC>:ZoomDefault<ESC>",{desc="zoom default"}) ]]

--term
map("n","<leader>tf","<ESC>:ToggleTerm direction=float<CR><ESC>",{desc="toggle a floating term"})
map("n","<leader>tt","<ESC>:ToggleTerm<CR><ESC>",{desc="toggle a split term"})

-- commenter
map("n","<leader>cl","gcc",{desc="comment line(s)"})
map("v","<leader>cl","gc",{desc="comment line(s)"})
map("n","<leader>cb","gbc",{desc="comment block"})
map("v","<leader>cb","gb",{desc="comment block"})

--markdown 

map("n","<leader>mi","<Cmd>MDListItemBelow<CR>",{desc="insert list item"})
map("n","<leader>mi","<Cmd>MDListItemBelow<CR>",{desc="insert list item above"})
