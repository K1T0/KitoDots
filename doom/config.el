;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Daniele Sica"
      user-mail-address "danielesica2006@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq org-fontify-quote-and-verse-blocks t)

;; Tema
(setq doom-theme 'doom-moonfly)
;; (setq doom-theme 'doom-tomorrow-night)


(doom-themes-org-config)
(after! 'doom-theme
  (setq! doom-themes-enable-bold t
         doom-themes-enable-italic t))

(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic)
  '(font-lock-comment-delimiter-face :weight bold :slant italic )
  '(font-lock-type-face :weight bold))

;; Line numbers"
(setq display-line-numbers-type t)

;; Fonts

(setq doom-font (font-spec :family "Source Code Pro" :size 14)
      doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 14)
      doom-big-font (font-spec :family "Source Code Pro" :size 24))

(setq global-prettify-symbols-mode t)

(setq rainbow-delimiters-max-face-count 6)


(setq shell-file-name "/bin/fish")

;; Org mode variables
(require 'org-superstar)
(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(after! org
  (setq org-directory "~/Org/"
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-superstar-headline-bullets-list '("◉" "●" "○" "◆" "●" "○" "◆")
        org-superstar-itembullet-alist '((?+ . ?➤) (?- . ?✦)) ; changes +/- symbols in item lists
        org-log-done 'time
        org-hide-emphasis-markers t
  )
)

(setq
   ;; org-fancy-priorities-list '("[A]" "[B]" "[C]")
   ;; org-fancy-priorities-list '("❗" "[B]" "[C]")
   org-fancy-priorities-list '("🟥" "🟧" "🟨")
   org-priority-faces
   '((?A :foreground "#ff6c6b" :weight bold)
     (?B :foreground "#98be65" :weight bold)
     (?C :foreground "#c678dd" :weight bold))
   org-agenda-block-separator 8411)



;; elfeed-org
(require 'elfeed)
(setq elfeed-goodies/entry-pane-size 0.9)
(setq message-kill-buffer-on-exit t)
(setq elfeed-show-refresh-function 'elfeed-show-refresh--mail-style)
(setq rmh-elfeed-org-files (list "~/.config/doom/elfeed.org"))
(defun elfeed-mark-all-as-read ()
      (interactive)
      (mark-whole-buffer)
      (elfeed-search-untag-all-unread))
(elfeed-org)

;; eww
(setq browse-url-browser-function 'eww-browse-url)

;; Navigazione fra finestre
(map! :map evil-window-map
      :leader
      ; Navigation
       "<left>"     #'evil-window-left
       "<down>"     #'evil-window-down
       "<up>"       #'evil-window-up
       "<right>"    #'evil-window-right)
;Go-End
(map! :vn "ge" #'end-of-buffer)
;navigazione nelle lineee
(map! :leader
      "e" #'end-of-line
      "a" #'beginning-of-visual-line)

;file tree
(map! :nv "<f3>"  #'treemacs)

;tabs
(map! :map comment-line-break-function
      :leader "cl" #'comment-line)

;eww
(map! :map eww
      :leader
      :desc "open link in eww"
      "o l" #'eww)

; writing-mode
(map! :map writroom-mode
      :leader
      :desc "Enter writing room mode"
      "g y" #'writeroom-mode)


;dired
(map! :leader
      :desc "Dired"
      "d d" #'dired
      :leader
      :desc "Dired jump to current"
      "d j" #'dired-jump
      (:after dired
        (:map dired-mode-map
         :leader
         :desc "Dired previews"
         "d p" #'dired-preview-mode
         :leader
         :desc "Dired view file"
         "d v" #'dired-view-file)))

(map! :after elfeed-search  ; ensure your keys are bound after defaults, if any
      :localleader
      :map elfeed-search-mode-map
      :prefix ("e" . "elfeed")
      :desc "Elfeed update"  :n "u" #'elfeed-update
      :desc "Elfeed readall" :n "R" #'elfeed-mark-all-as-read)

;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!
(evil-define-key 'normal dired-mode-map
   (kbd "<up>") 'dired-previous-line
   (kbd "<down>") 'dired-next-line)

;; Python mode TAB
(add-hook 'python-mode-hook
	    (lambda ()
	    (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(setq indent-tabs-mode t)

;; PDF-viewer options
(add-hook 'pdf-tools-enabled-hook

'pdf-view-midnight-minor-mode)

(add-hook 'pdf-tools-enabled-hook

'hide-mode-line-mode)

;; Dired


;; (require 'ranger)
;; (setq ranger-override-dired-mode t)
;; (setq ranger-cleanup-eagerly t)
;; ;; (setq ranger-hide-cursor t)
;; (setq ranger-show-hidden t)
;; (setq ranger-modify-header t)
;; (setq ranger-max-preview-size 1000)
;; (setq ranger-show-literal t)

(require 'dired-preview)

;; Default values for demo purposes
(setq dired-preview-delay 0.3)
(setq dired-preview-max-size (expt 2 20))
(setq dired-preview-ignored-extensions-regexp
        (concat "\\."
                "\\(gz\\|"
                "zst\\|"
                "tar\\|"
                "xz\\|"
                "rar\\|"
                "zip\\|"
                "iso\\|"
                "mp4\\|"
                "mkv\\|"
                "epub"
                "\\)"))

(dired-preview-global-mode 1)

(setq dired-mouse-drag-files t)
(setq mouse-drag-and-drop-region-cross-program t)

;; Smooth scrolling
(setq scroll-conservatively 101)
