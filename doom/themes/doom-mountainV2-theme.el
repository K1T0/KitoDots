;;; doom-mountainV2-theme.el ends here
;;; doom-mountainV2-theme.el --- inspired by Atom MountainV2 Dark -*- lexical-binding: t; no-byte-compile: t; -*-
(require 'doom-themes)

(defgroup doom-mountainV2-theme nil
  "Options for the `doom-mountainV2' theme."
  :group 'doom-themes)

(defcustom doom-mountainV2-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-comment-bg doom-mountainV2-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-mountainV2-theme
  :type '(choice integer boolean))

;;
(def-doom-theme doom-mountainV2
  "A dark theme inspired by Atom MountainV2 Dark"

  ;; name        default   256       16

  ((bg         '("#0f0f0f" "#191919" nil))
   (bg-alt     '("#191919" "#262626" nil))
   (bg-alt2    '("#262626" "#262626" nil)) ; bg2 (for region, selection etc.)

   (base0      '("#0f0f0f" "#0f0f0f" "black"      )) ; (self-defined)
   (base1      '("#191919" "#191919" "brightblack")) ; bg0_h
   (base2      '("#262626" "#262626" "brightblack")) ; bg0
   (base3      '("#393939" "#393939" "brightblack")) ; bg1
   (base4      '("#4c4c4c" "#4c4c4c" "brightblack")) ; bg3
   (base5      '("#767676" "#767676" "brightblack")) ; bg4
   (base6      '("#a0a0a0" "#a0a0a0" "brightblack")) ; gray
   (base7      '("#bfbfbf" "#bfbfbf" "brightblack")) ; fg2
   (base8      '("#cacaca" "#cacaca" "brightwhite")) ; fg0
   (fg         '("#f0f0f0" "#f0f0f0" "brightwhite")) ; fg/fg1
   (fg-alt     '("#e7e7e7" "#e7e7e7" "brightwhite")) ; fg2

   (grey       '("#4c4c4c" nil "brightblack"))
   (red        '("#ac8a8c" nil "red"))
   (salmon     '("#c49ea0" nil "red"))
   (magenta    '("#ac8aac" nil "magenta"))
   (violet     '("#c49ec4" nil "brightmagenta"))
   (orange     '("#c6a679" nil "orange"))
   (yellow     '("#aca98a" nil "yellow"))
   (green      '("#9ec49f" nil "green"))
   (dark-green '("#8aac8b" nil "green"))
   (dark-blue  '("#8f8aac" nil "brightblue"))
   (blue       '("#a39ec4" nil "brightblue"))
   (dark-cyan  '("#8aabac" nil "brightcyan"))
   (cyan       '("#9ec3c4" nil "brightcyan"))
   (teal       '("#9ec3c4" nil "brightcyan"))
   ;; face categories -- required for all themes
   (highlight      red)
   (vertical-bar   (doom-darken bg 0.25))
   (selection      base4)
   (builtin        salmon)
   (comments       (if doom-mountainV2-brighter-comments dark-cyan base5))
   (doc-comments   (doom-lighten (if doom-mountainV2-brighter-comments dark-cyan base5) 0.25))
   (constants      orange)
   (functions      dark-blue)
   (keywords       magenta)
   (methods        blue)
   (operators      fg)
   (type           yellow)
   (strings        dark-green)
   (variables      red)
   (numbers        orange)
   (region         selection)
   (error          red)
   (warning        yellow)
   (success        dark-green)
   (vc-modified    orange)
   (vc-added       dark-green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg-alt) "black" "black"))
   (-modeline-bright doom-mountainV2-brighter-modeline)
   (-modeline-pad
    (when doom-mountainV2-padded-modeline
      (if (integerp doom-mountainV2-padded-modeline) doom-mountainV2-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt (doom-blend violet base4 (if -modeline-bright 0.5 0.2)))
   (modeline-bg
    (if -modeline-bright
        (doom-darken base3 0.1)
      base1))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken base3 0.05)
      base1))
   (modeline-bg-inactive   `(,(doom-darken (car bg) 0.05) ,@(cdr base1)))
   (modeline-bg-inactive-l (doom-darken bg-alt 0.1)))


  ;;;; Base theme face overrides
  (((font-lock-comment-face &override)
    :background (if doom-mountainV2-comment-bg (doom-lighten bg 0.05)))
   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground fg)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if -modeline-bright base8 highlight))

   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground fg)
   (css-selector             :foreground red)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-darken bg 0.1))
   ;;;; outline <built-in>
   ((outline-1 &override) :foreground fg :weight 'ultra-bold)
   ((outline-2 &override) :foreground (doom-blend fg blue 0.35))
   ((outline-3 &override) :foreground (doom-blend fg blue 0.7))
   ((outline-4 &override) :foreground blue)
   ((outline-5 &override) :foreground (doom-blend magenta blue 0.2))
   ((outline-6 &override) :foreground (doom-blend magenta blue 0.4))
   ((outline-7 &override) :foreground (doom-blend magenta blue 0.6))
   ((outline-8 &override) :foreground fg)
   ;;;; org <built-in>
   (org-block            :background (doom-darken bg-alt 0.04))
   (org-block-begin-line :foreground base4 :slant 'italic :background (doom-darken bg 0.04))
   (org-ellipsis         :underline nil :background bg    :foreground red)
   ((org-quote &override) :background base1)
   (org-hide :foreground bg)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l))))

  ;;;; Base theme variable overrides-
  ;; ()
  )

;;; doom-mountainV2-theme.el ends here
;;; doom-mountainV2-theme.el --- inspired by Atom MountainV2 Dark -*- lexical-binding: t; no-byte-compile: t; -*-
(require 'doom-themes)

(defgroup doom-mountainV2-theme nil
  "Options for the `doom-mountainV2' theme."
  :group 'doom-themes)

(defcustom doom-mountainV2-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-comment-bg doom-mountainV2-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-mountainV2-theme
  :type '(choice integer boolean))

;;
(def-doom-theme doom-mountainV2
  "A dark theme inspired by Atom MountainV2 Dark"

  ;; name        default   256       16
  ((bg         '("#2b303b" nil       nil            ))
   (bg-alt     '("#232830" nil       nil            ))
   (base0      '("#1B2229" "black"   "black"        ))
   (base1      '("#1c1f24" "#1e1e1e" "brightblack"  ))
   (base2      '("#202328" "#2e2e2e" "brightblack"  ))
   (base3      '("#2F3237" "#2F3237" "brightblack"  ))
   (base4      '("#4f5b66" "#4f5b66" "brightblack"  ))
   (base5      '("#65737E" "#65737E" "brightblack"  ))
   (base6      '("#73797e" "#6b6b6b" "brightblack"  ))
   (base7      '("#9ca0a4" "#979797" "brightblack"  ))
   (base8      '("#DFDFDF" "#dfdfdf" "white"        ))
   (fg         '("#c0c5ce" "#c0c5ce" "brightwhite"  ))
   (fg-alt     '("#c0c5ce" "#c0c5ce" "white"        ))

   (grey       base4)
   (red        '("#BF616A" "#BF616A" "red"          ))
   (orange     '("#D08770" "#D08770" "brightred"    ))
   (green      '("#A3BE8C" "#A3BE8C" "green"        ))
   (blue       '("#8FA1B3" "#8FA1B3" "brightblue"   ))
   (violet     '("#b48ead" "#b48ead" "brightmagenta"))
   (teal       '("#4db5bd" "#44b9b1" "brightgreen"  ))
   (yellow     '("#ECBE7B" "#ECBE7B" "yellow"       ))
   (dark-blue  '("#2257A0" "#2257A0" "blue"         ))
   (magenta    '("#c678dd" "#c678dd" "magenta"      ))
   (cyan       '("#46D9FF" "#46D9FF" "brightcyan"   ))
   (dark-cyan  '("#5699AF" "#5699AF" "cyan"         ))

   ;; face categories -- required for all themes
   (highlight      orange)
   (vertical-bar   (doom-darken bg 0.25))
   (selection      base4)
   (builtin        orange)
   (comments       (if doom-mountainV2-brighter-comments dark-cyan base5))
   (doc-comments   (doom-lighten (if doom-mountainV2-brighter-comments dark-cyan base5) 0.25))
   (constants      orange)
   (functions      blue)
   (keywords       violet)
   (methods        blue)
   (operators      fg)
   (type           yellow)
   (strings        green)
   (variables      red)
   (numbers        orange)
   (region         selection)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg-alt) "black" "black"))
   (-modeline-bright doom-mountainV2-brighter-modeline)
   (-modeline-pad
    (when doom-mountainV2-padded-modeline
      (if (integerp doom-mountainV2-padded-modeline) doom-mountainV2-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt (doom-blend violet base4 (if -modeline-bright 0.5 0.2)))
   (modeline-bg
    (if -modeline-bright
        (doom-darken base3 0.1)
      base1))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken base3 0.05)
      base1))
   (modeline-bg-inactive   `(,(doom-darken (car bg) 0.05) ,@(cdr base1)))
   (modeline-bg-inactive-l (doom-darken bg-alt 0.1)))


  ;;;; Base theme face overrides
  (((font-lock-comment-face &override)
    :background (if doom-mountainV2-comment-bg (doom-lighten bg 0.05)))
   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground fg)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if -modeline-bright base8 highlight))

   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground fg)
   (css-selector             :foreground red)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-darken bg 0.1))
   ;;;; outline <built-in>
   ((outline-1 &override) :foreground fg :weight 'ultra-bold)
   ((outline-2 &override) :foreground (doom-blend fg blue 0.35))
   ((outline-3 &override) :foreground (doom-blend fg blue 0.7))
   ((outline-4 &override) :foreground blue)
   ((outline-5 &override) :foreground (doom-blend magenta blue 0.2))
   ((outline-6 &override) :foreground (doom-blend magenta blue 0.4))
   ((outline-7 &override) :foreground (doom-blend magenta blue 0.6))
   ((outline-8 &override) :foreground fg)
   ;;;; org <built-in>
   (org-block            :background (doom-darken bg-alt 0.04))
   (org-block-begin-line :foreground base4 :slant 'italic :background (doom-darken bg 0.04))
   (org-ellipsis         :underline nil :background bg    :foreground red)
   ((org-quote &override) :background base1)
   (org-hide :foreground bg)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l))))

  ;;;; Base theme variable overrides-
  ;; ()
  )

;;; doom-mountainV2-theme.el ends her;;; doom-mountainV2-theme.el --- inspired by Atom MountainV2 Dark -*- lexical-binding: t; no-byte-compile: t; -*-
(require 'doom-themes)

(defgroup doom-mountainV2-theme nil
  "Options for the `doom-mountainV2' theme."
  :group 'doom-themes)

(defcustom doom-mountainV2-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-comment-bg doom-mountainV2-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-mountainV2-theme
  :type 'boolean)

(defcustom doom-mountainV2-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-mountainV2-theme
  :type '(choice integer boolean))

;;
(def-doom-theme doom-mountainV2
  "A dark theme inspired by Atom MountainV2 Dark"

  ;; name        default   256       16
  ((bg         '("#2b303b" nil       nil            ))
   (bg-alt     '("#232830" nil       nil            ))
   (base0      '("#1B2229" "black"   "black"        ))
   (base1      '("#1c1f24" "#1e1e1e" "brightblack"  ))
   (base2      '("#202328" "#2e2e2e" "brightblack"  ))
   (base3      '("#2F3237" "#2F3237" "brightblack"  ))
   (base4      '("#4f5b66" "#4f5b66" "brightblack"  ))
   (base5      '("#65737E" "#65737E" "brightblack"  ))
   (base6      '("#73797e" "#6b6b6b" "brightblack"  ))
   (base7      '("#9ca0a4" "#979797" "brightblack"  ))
   (base8      '("#DFDFDF" "#dfdfdf" "white"        ))
   (fg         '("#c0c5ce" "#c0c5ce" "brightwhite"  ))
   (fg-alt     '("#c0c5ce" "#c0c5ce" "white"        ))

   (grey       base4)
   (red        '("#BF616A" "#BF616A" "red"          ))
   (orange     '("#D08770" "#D08770" "brightred"    ))
   (green      '("#A3BE8C" "#A3BE8C" "green"        ))
   (blue       '("#8FA1B3" "#8FA1B3" "brightblue"   ))
   (violet     '("#b48ead" "#b48ead" "brightmagenta"))
   (teal       '("#4db5bd" "#44b9b1" "brightgreen"  ))
   (yellow     '("#ECBE7B" "#ECBE7B" "yellow"       ))
   (dark-blue  '("#2257A0" "#2257A0" "blue"         ))
   (magenta    '("#c678dd" "#c678dd" "magenta"      ))
   (cyan       '("#46D9FF" "#46D9FF" "brightcyan"   ))
   (dark-cyan  '("#5699AF" "#5699AF" "cyan"         ))

   ;; face categories -- required for all themes
   (highlight      orange)
   (vertical-bar   (doom-darken bg 0.25))
   (selection      base4)
   (builtin        orange)
   (comments       (if doom-mountainV2-brighter-comments dark-cyan base5))
   (doc-comments   (doom-lighten (if doom-mountainV2-brighter-comments dark-cyan base5) 0.25))
   (constants      orange)
   (functions      blue)
   (keywords       violet)
   (methods        blue)
   (operators      fg)
   (type           yellow)
   (strings        green)
   (variables      red)
   (numbers        orange)
   (region         selection)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg-alt) "black" "black"))
   (-modeline-bright doom-mountainV2-brighter-modeline)
   (-modeline-pad
    (when doom-mountainV2-padded-modeline
      (if (integerp doom-mountainV2-padded-modeline) doom-mountainV2-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt (doom-blend violet base4 (if -modeline-bright 0.5 0.2)))
   (modeline-bg
    (if -modeline-bright
        (doom-darken base3 0.1)
      base1))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken base3 0.05)
      base1))
   (modeline-bg-inactive   `(,(doom-darken (car bg-alt) 0.05) ,@(cdr base1)))
   (modeline-bg-inactive-l (doom-darken bg 0.1)))


  ;;;; Base theme face overrides
  (((font-lock-comment-face &override)
    :background (if doom-mountainV2-comment-bg (doom-lighten bg 0.05)))
   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground fg)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if -modeline-bright base8 highlight))

   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground fg)
   (css-selector             :foreground red)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-darken bg 0.1))
   ;;;; outline <built-in>
   ((outline-1 &override) :foreground fg :weight 'ultra-bold)
   ((outline-2 &override) :foreground (doom-blend fg blue 0.35))
   ((outline-3 &override) :foreground (doom-blend fg blue 0.7))
   ((outline-4 &override) :foreground blue)
   ((outline-5 &override) :foreground (doom-blend magenta blue 0.2))
   ((outline-6 &override) :foreground (doom-blend magenta blue 0.4))
   ((outline-7 &override) :foreground (doom-blend magenta blue 0.6))
   ((outline-8 &override) :foreground fg)
   ;;;; org <built-in>
   (org-block            :background (doom-darken bg-alt 0.04))
   (org-block-begin-line :foreground base4 :slant 'italic :background (doom-darken bg 0.04))
   (org-ellipsis         :underline nil :background bg    :foreground red)
   ((org-quote &override) :background base1)
   (org-hide :foreground bg)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l))))

  ;;;; Base theme variable overrides-
  ;; ()
  )

;;; doom-mountainV2-theme.el ends here
