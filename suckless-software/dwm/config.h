// gaps
static const unsigned int borderpx  = 3;
static const unsigned int snap      = 32;  /* snap pixel */
static const unsigned int gappih    = 30;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 30;       /* vert inner gap between windows */
static const unsigned int gappoh    = 25;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 25;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
// bars
static const int showbar            = 1;        
static const int topbar             = 1;
//font
static const char *fonts[]     = {"Mononoki Nerd Font:size=10:antialias=true:autohint=true",
                                  "Hack:size=8:antialias=true:autohint=true",
                                  "JoyPixels:size=10:antialias=true:autohint=true"};
static const char dmenufont[]       = "Mononoki Nerd Font:size=9:antialias=true:autohint=true";
//colors
static const char col_gray1[]       = "#0F0F0F";
static const char col_gray2[]       = "#282c20";
static const char col_gray3[]       = "#282c10";
static const char col_gray4[]       = "#2e2c20";
static const char col_gray5[]       = "#89ddff";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray5, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray5, col_gray5 },
};
//tags
//
static const char *tags[] = { " ", " ", " ", " ", " ", " ", " ", " ", " " };
static const unsigned int ulinepad	= 4;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const char *tagsel[][2] = {
	{ "#9EC49F","#0F0F0F" },
	{ "#8A98AC","#0F0F0F" },
	{ "#89ddff","#0F0F0F" },
	{ "#f0f0f0","#0F0F0F" },
	{ "#ffd556","#0F0F0F" },
	{ "#AC8AAC","#0F0F0F" },
	{ "#AC8A8C","#0F0F0F" },
	{ "#00dddd","#0F0F0F" },
	{ "#9EC49F","#0F0F0F" },
};

static const Rule rules[] = {
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            0,           -1 },
	{ "firefox",  NULL,       NULL,       0,			      0,           1 },
	{ NULL,       NULL,   "scratchterm",   0,            1,           -1,       's' },
	{ NULL,       NULL,   "scratchnote",   0,            1,           -1,       's' },
	{ NULL,       NULL,   "scratchcalc",   0,            1,           -1,       's' },
	{ NULL,       NULL,   "scratchfile",   0,            1,           -1,       's' },
	{ NULL,       NULL,   "scratchpad",   0,            1,           -1,       's' },
};

/* layout(s) */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },  //  [> first entry is default <]
	{ "[M]",      monocle },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "|M|",      centeredmaster }, /* centered master */ 
	{ ">M>",      centeredfloatingmaster }, /* centered master */
	{ "[@]",      spiral },
	{ "TTT",      bstack },
	{ ":::",      gaplessgrid },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};
  // Layouts aviable but not used
	//{ "===",      bstackhoriz },
	//{ "HHH",      grid },
	//{ "###",      nrowgrid },
	//{ "---",      horizgrid },
	//{ "[\\]",     dwindle },
	//{ "H[]",      deck },

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, 

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }


/* dmenu */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
/* If you are using the standard dmenu program, use the following. */
static const char *dmenucmd[]    = { "rofi","-show","drun", "-p", "Run: ", NULL };
/* commands */
static const char *termcmd[]  = { "alacritty", NULL };

// scratchpads
static const char *scratchterm[] = {"s", "alacritty", "-t", "scratchpadterm", NULL}; 
static const char *scratchnote[] = {"s", "alacritty", "-t", "scratchpadnote","-e","sncli", NULL}; 
static const char *scratchcalc[] = {"s", "qalculate-gtk", "--title=scratchpadcalc", NULL}; 
static const char *scratchfile[] = {"s", "alacritty", "-t", "scratchpadfile","-e","lf", NULL}; 
//static const char *scratchhelp[] = {"s", "alacritty", "-t", "scratchpadhelp","-e","awk '{if($NR>=96 && $NR<=163 ) print $0}' '$HOME/.config/suckless-software/dwm/config.h' | nvim", NULL}; 

//Keys
#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	/* command keybiding */
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = dmenucmd } },
	{ MODKEY,		                XK_Return, spawn,          {.v = termcmd } },

	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Left,   focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Right,  focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Left,   movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Right,  movestack,      {.i = -1 } },
	{ MODKEY,                       XK_Up,     incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_Down,   incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Up,     setmfact,       {.f = -0.05} },
	{ MODKEY|ShiftMask,             XK_Down,   setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_B, zoom,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },

	/* Layouts */ 
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_u,      setlayout,      {.v = &layouts[3]} }, 
	/* centered master */ 
	{ MODKEY|ShiftMask,             XK_o,      setlayout,      {.v = &layouts[4]} }, 
	{ MODKEY|ShiftMask,		          XK_Tab,    cyclelayout,    {.i = -1 } },
	{ MODKEY,		                    XK_Tab,    cyclelayout,    {.i = +1 } },
	{ MODKEY,                       XK_space,  togglefullscr,  {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
  
  // gaps
  { MODKEY|ALTKEY,              XK_Up,     incrgaps,       {.i = +1 } },
	{ MODKEY|ALTKEY,              XK_Down,   incrgaps,       {.i = -1 } },
	{ MODKEY|ALTKEY,              XK_Right,  incrigaps,      {.i = +1 } },
	{ MODKEY|ALTKEY,              XK_Left,   incrigaps,      {.i = -1 } },
	{ MODKEY|ALTKEY|ShiftMask,    XK_Up,     incrogaps,      {.i = +1 } },
	{ MODKEY|ALTKEY|ShiftMask,    XK_Down,   incrogaps,      {.i = -1 } },
	{ MODKEY|ALTKEY|ShiftMask,    XK_Right,  incrihgaps,     {.i = +1 } },
	{ MODKEY|ALTKEY|ShiftMask,    XK_Left,   incrihgaps,     {.i = -1 } },
	//{ MODKEY|ALTKEY,              XK_7,      incrivgaps,     {.i = +1 } },
	//{ MODKEY|ALTKEY|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	//{ MODKEY|ALTKEY,              XK_8,      incrohgaps,     {.i = +1 } },
	//{ MODKEY|ALTKEY|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	//{ MODKEY|ALTKEY,              XK_9,      incrovgaps,     {.i = +1 } },
	//{ MODKEY|ALTKEY|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|ALTKEY,              XK_0,      togglegaps,     {0} },
	{ MODKEY|ALTKEY,              XK_space,      defaultgaps,    {0} },
  
  // scratchpads
  { ControlMask| ALTKEY,        XK_t,  togglescratch,  {.v = scratchterm } },
  { ControlMask| ALTKEY,        XK_n,  togglescratch,  {.v = scratchnote } },
  { ControlMask| ALTKEY,        XK_f,  togglescratch,  {.v = scratchfile } },
  { ControlMask| ALTKEY,        XK_c,  togglescratch,  {.v = scratchcalc } },
  //{ ControlMask| ALTKEY,        XK_l,  togglescratch,  {.v = scratchhelp } },

  // tag and focus
  { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
  { MODKEY|ShiftMask,             XK_r,      quit,           {1} },
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	//{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

static Signal signals[]={
  {1,     togglescratch,    {.v = scratchterm}},
  {2,     togglescratch,    {.v = scratchcalc}},
  {3,     togglescratch,    {.v = scratchfile}},
  {4,     togglescratch,    {.v = scratchnote}},
};
