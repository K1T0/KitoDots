#!/usr/bin/env bash

config="$HOME/.config/suckless-software/dwm/config.h"

awk -v start=97 -v end=177 '{ if( NR >= start && NR <= end && $1 != "//{") print $0}' $config > /tmp/keys.c

nvim /tmp/keys.c


