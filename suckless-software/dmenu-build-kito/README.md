# Dmenu-build-kito

1. [What is dmenu?](#what-is-dmenu?)
2. [What is dmenu-build-kito](#what-is-dmenu-build-kito)
3. [Mouse functionality](#mouse-functionality)
4. [Available flags](#available-flags)

## What is dmenu?

dmenu is a dynamic menu for X, originally designed for dwm. It manages large
numbers of user-defined menu items efficiently.  It is a very powerful tool
that allows you to pipe information into it.  This makes dmenu a perfect
utility to incorporate into your scripting.

## What is dmenu-build-kito

dmenu-distroube is my patched build of dmenu. It is very functional and
lightway, so i always use it in my shell scripts.
The patch i use are:

+ [Dmenu-border](https://tools.suckless.org/dmenu/patches/border/).Adds border
	around dmenu window
+ [Dmenu-center](https://tools.suckless.org/dmenu/patches/center/).Allow dmenu
	to run in a rofi style in the middle of the screen
+ [Dmenu-fuzzyhighlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight/).Fuzzy matches gets highlighted
+ [Dmenu-fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/).Adds support for fuzzy-matching
+ [Dmenu-grid](https://tools.suckless.org/dmenu/patches/grid/). Allow to choose the number of lines and columns of dmenu
+ [Dmenu-lineheight](https://tools.suckless.org/dmenu/patches/line-height/).Allow to choose the minimun height of the line
+ [Dmenu-morecolor](https://tools.suckless.org/dmenu/patches/morecolor/). Creates a color option for use the entries adjacent to the selection
+ [Dmenu-mousesupport](https://tools.suckless.org/dmenu/patches/mouse-support/).Enable to use the mouse with dmenu
+ [Dmenu-numbers](https://tools.suckless.org/dmenu/patches/numbers/).Displays number of matched and total items in top right corner

## Mouse functionality

Mouse actions supported:

+ Left-mouse click:
  + On prompt and input field: clear input text and selection.
  + In horizontal and vertical mode on item: select and output item (same as pressing enter).
  + In horizontal mode on arrows: change items to show left or right.
+ Ctrl-left-mouse click: multisel modifier.
+ Right-mouse click: close.
+ Middle-mouse click:
  + Paste current selection.
  + While holding shift: paste primary selection.
+ Scroll up:
  + In horizontal mode: same as left-clicking on left arrow.
  + In vertical mode: show items above.
+ Scroll down:
  + In horizontal mode: same as left-clicking on right arrow.
  + In vertical mode: show items below.

## Available flags

+ [-l lines]
+ [-g columns]
+ [-p prompt]
+ [-fn font]
+ [-m monitor]
+ [-h height]
+ [-w windowid]
+ [-c center view]
+ [-nb color]
+ [-nf color]
+ [-sb color]
+ [-sf color]
+ [-nhb color]
+ [-nhf color]
+ [-shb color]
+ [-shf color]
