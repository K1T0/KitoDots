#!/bin/bash

#launcher="dmenu -i -p 'Edit config file:"
launcher="rofi -dmenu -i -theme kito-dmenu-lines.rasi "

editor="emacsclient -c"

declare options=("alacritty
openbox
bash
dunst
neovim
picom
polybar
qutebrowser
zsh
fish
dwm
quit")

choice=$(echo -e "${options[@]}" | $launcher -p 'Edit config file: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	openbox)
		choice="$HOME/.config/openbox/rc.xml"
	;;
	bash)
		choice="$HOME/.bashrc"
	;;
	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;
	picom)
		choice="$HOME/.config/picom/picom.conf"
	;;
	polybar)
		choice="$HOME/.config/polybar/config.ini"
	;;
	qutebrowser)
		choice="$HOME/.config/qutebrowser/config.py"
	;;
	zsh)
		choice="$HOME/.zshrc"
	;;
  fish)
    choise="$HOME/.config/fish/config.fish"
  ;;
  	dwm)
	choise="$HOME/.config/suckless-software/dwm/config.h"
	;;
	*)
		exit 1
	;;
esac

$editor "$choice"
