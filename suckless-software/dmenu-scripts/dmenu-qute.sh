#!/usr/bin/env bash

set -euo pipefail

DMBROWSER="${BROWSER:-qutebrowser}"

#launcher="dmenu -i -l 20 -p 'Qutebrowser open:'"
launcher="rofi -theme kito-dmenu-lines.rasi -dmenu -i "

# Show from what browser the url entry is from
SHOW_BROWSER=1

HISTORY_CACHE_FILE="${HOME}/.cache/dmbrowserhistory"


# Defining location of quickmarks file
QMFILE="$HOME/.config/qutebrowser/quickmarks"

# cercare nelle cronologie dei browser
#
# Do query against sqlite3 database expecting three columns (browsername, title, url)
# ARGS: "browsername" "file" "query"

# Read array of options to choose.
readarray -t qmarks < "${QMFILE}"

# Sort the bookmark and quickmark lists so that the url is the last field.
# We will awk print the last field later.
# History list is formed by grep'ing "http" from the history table.
qmlist=$(printf '%s\n' "${qmarks[@]}" | awk '{print ""$1":  "$NF}' | sort)


# Piping the lists into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
# The urls are listed quickmarks first, then the SEPARATOR, and then bookmarks and lastly history
choice=$(echo -e "$qmlist" | $launcher -p "Search :" "$@" ) || exit

# What to do if the separator is chosen from the list.
# We simply launch qutebrowser without any url arguments.
# shellcheck disable=SC2154
if [ "$choice" ]; then
  if [[ $qmlist =~ $choice ]]; then
    echo "match"
    url=$(echo "${choice}" | awk '{print $NF}') || exit
    nohup "${DMBROWSER}" "$url" >/dev/null 2>&1 &
  else
    url=$(echo "${choice}") || exit
    nohup "${DMBROWSER}" "$url" >/dev/null 2>&1 &
# What to do if we just escape without choosing anything.
   fi
else
    echo "Program terminated." && exit 0
fi
