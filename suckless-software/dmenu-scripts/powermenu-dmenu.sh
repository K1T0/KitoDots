#!/bin/sh

#LAUNCHER=' dmenu -i -l 4  "'
LAUNCHER='rofi -dmenu -theme kito-dmenu-lines.rasi '

DM=lxdm-plymouth
option=`echo -e "Riavvia\nSpegni\nLogout" | $LAUNCHER -p "  :"`
if [ ${#option} -gt 0 ]
then
    case $option in
      Logout)
	    loginctl terminate-session $XDG_SESSION_ID
	;;
      Riavvia)
        sudo shutdown -rh 0
        ;;
      Spegni)
        sudo shutdown -h 0
        ;;
      *)
        ;;
    esac
fi
