#!/bin/sh
vol="$(amixer sget Master | grep 'Right:' | awk -F'[][]' '{ print $2 }')"

if [ "$vol" -gt "70" ]; then
	icon="  "
elif [ "$vol" -lt "30" ]; then
	icon="  "
else
	icon="  "
fi

echo "$icon$vol"
