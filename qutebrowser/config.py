import mountain.draw
# config.py for qutebrowser of istael. Read my README.org
config.load_autoconfig(False)
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save'}

#Cookies

config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')

#user agent
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://docs.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://drive.google.com/*')

# Load images automatically in web pages.
# Type: Bool
config.set('content.images', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome-devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome://*/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'qute://*/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'https://animeworld.tv/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'file:///home/istael/.config/startpage/actual/*/*')

config.set('content.blocking.enabled',False,'file:///home/istael/.config/startpage/actual/*/*')
#geolocation

config.set('content.geolocation',True,'file:///home/istael/.config/startpage/actual/*/*')
#notifications


config.set('content.notifications.enabled', True, 'https://www.reddit.com')
config.set('content.notifications.enabled', True, 'https://www.youtube.com')
config.set('content.notifications.enabled', True, 'file://*')

c.downloads.location.directory = '/home/istael/Scaricati/'

#bars
c.tabs.show = 'multiple'
c.statusbar.show = 'never'

#startpage
c.url.default_page = 'file:///home/istael/.config/startpage/actual/index.html'
c.url.start_pages = 'file:///home/istael/.config/startpage/actual/index.html'

c.colors.completion.category.bg = 'qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #000000, stop:1 #232429)'


# Konda means mountain in telugu :P
# Don't worry, I know it's a bad joke too...
mountain.draw.konda(c, {
    'spacing': {
        'vertical': 6,
        'horizontal': 8
    }
})

c.fonts.hints = '9pt NotoSerifNerdFont'
c.fonts.keyhint = '9pt Source Code Pro'
c.fonts.prompts = '9pt Source Code Pro'
c.fonts.downloads = '9pt Source Code Pro'
c.fonts.statusbar = '9pt Source Code Pro'
c.fonts.contextmenu = '9pt Source Code Pro'
c.fonts.messages.info = '9pt Source Code Pro'
c.fonts.debug_console = '9pt Source Code Pro'
c.fonts.completion.entry = '9pt Source Code Pro'
c.fonts.completion.category = '9pt Source Code Pro'
c.fonts.web.family.serif = 'Source Code Pro'
c.fonts.web.family.sans_serif = 'Source Code Pro'
c.fonts.web.family.standard = 'Source Code Pro'



c.editor.command = ['/usr/bin/nvim', '-f', '{}']


#keybindings
# Bindings for normal mode
config.bind('M', 'hint links spawn mpv {hint-url}')
config.bind('Z', 'hint links spawn st -e youtube-dl {hint-url}')
config.bind('t', 'set-cmd-text -s :open -t')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')


config.bind(',bo', 'set-cmd-text -s :bookmark-load ')
config.bind(',ba', 'bookmark-add')

# return to the homepage
config.bind('D', ':open file:///home/istael/.config/startpage/actual/index.html')
